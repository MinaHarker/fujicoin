<?php

namespace App\Controller;

use App\Service\ApiUtils;
use App\Service\JsonRpcClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class ResourceController extends AbstractController
{
    public function press(): Response
    {
        return $this->render('resources/press.html.twig');
    }
    public function promotionalGraphics(): Response
    {
        return $this->render('resources/promotional_graphics.html.twig');
    }
    public function blockchainExplorers(): Response
    {
        return $this->render(
            'resources/blockchain_explorers.html.twig'
        );
    }
    public function api(JsonRpcClient $fujicoin, ApiUtils $utils): Response
    {
        $txoutsetinfo = $fujicoin->gettxoutsetinfo();
        $getmininginfo = $fujicoin->getmininginfo();
        $networkhashps = $getmininginfo['networkhashps'] / 1000000;

        $height = $txoutsetinfo['height'];
        $reward = $utils->reward($txoutsetinfo['height']);
        $total_amount = $txoutsetinfo['total_amount'];
        $difficulty = $getmininginfo['difficulty'];

        return $this->render('resources/api.html.twig', [
            'api' => [
                'height' => $height,
                'reward' => $reward,
                'total_amount' => $total_amount,
                'difficulty' => $difficulty,
                'networkhashps' => $networkhashps]
        ]);
    }

    public function node(TranslatorInterface $translator, JsonRpcClient $fujicoin, ApiUtils $utils): Response
    {
        $getpeerinfo = $fujicoin->getpeerinfo();
        $array = [];

        // Parse array to get the ip addresses for the node informations
        foreach ($getpeerinfo as $item) {
            $array[$item['addr']] = [$translator->trans('node.closed'), $item['subver']];
            if ($utils->whichIp($item['addr']) === "ipv4") {
                // separate ip from the port
                $ip = explode(':', $item['addr']);
                // if the port exist
                if (isset($ip[1])) {
                    if ($ip[1] == "3777") {
                        $array[$item['addr']] = [$translator->trans('node.open'), $item["subver"]];
                    }
                } else {
                    $array[$item['addr']] = [$translator->trans('node.open'), $item["subver"]];
                }
            }
        }

        return $this->render('resources/node.html.twig', [
            'peers_info' => $array
        ]);
    }

    public function totalSupply(JsonRpcClient $fujicoin): Response
    {
        $tx_out_set_info = $fujicoin->gettxoutsetinfo();

        return $this->render('resources/total_supply.html.twig', [
            'total_amount' => $tx_out_set_info['total_amount']]);
    }
}
