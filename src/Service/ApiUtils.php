<?php

namespace App\Service;

class ApiUtils
{
    public function reward($height)
    {
        // S-Curve theory: Gompertz curve
        $k = 10000000000.0; // Targrt 100 years ago
        $b = 0.1;
        $c = 2.0;
        $ybnum = 365 * 24 * 60.0;
        $x0 = -0.3;
        $x = $x0 + $height / $ybnum / 10.0;
        $e = exp(-$c * $x);
        $r = -$k * $c * log($b) * $e * pow($b, $e) / $ybnum / 10.0;
        if ($r >= 10) {
            $r = floor($r);
        }
        return $r;
    }

    public function whichIp($address)
    {
        if ($address[0] === '[') {
            return "ipv6";
        } else {
            return "ipv4";
        }
    }
}
