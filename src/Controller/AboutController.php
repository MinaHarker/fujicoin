<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class AboutController extends AbstractController
{
    public function whatIsFujicoin(): Response
    {
        // Array to do a loop in the twig file -> less code in this last one.
        $specifications = [];
        $specifications['algorithm'] = ['title', 'text1'];
        $specifications['block_interval'] = ['title', 'text1'];
        $specifications['difficulty_retarget'] = ['title', 'text1'];
        $specifications['total_coins_amount'] = ['title', 'text1'];
        $specifications['premine'] = ['title', 'text1'];
        $specifications['block_reward_curve'] = ['title', 'text1'];
        $specifications['currency_unit'] = ['title', 'text1'];

        $about = ['description', 's_curve', 'specifications'];

        return $this->render('about/what_is_fujicoin/what_is_fujicoin.html.twig', [
            'specifications' => $specifications,
            'about' => $about
        ]);
    }
    public function team(): Response
    {
        return $this->render('about/team.html.twig');
    }
    public function fujicoinFoundation(): Response
    {
        return $this->render('about/fujicoin_foundation.html.twig');
    }
}
