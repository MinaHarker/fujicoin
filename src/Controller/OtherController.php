<?php

namespace App\Controller;

use App\Service\ApiUtils;
use App\Service\JsonRpcClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class OtherController extends AbstractController
{
    public function home(): Response
    {
        // Array to do a loop in the twig file -> less code in this last one
        $market = [];
        $market['crex24'] = ['url' => 'https://crex24.com/exchange/FJC-BTC'];
        $market['ataix'] = ['url' => 'https://trade.ataix.com/FJC-BTC'];
        $market['unnamed'] = ['url' => 'https://www.unnamed.exchange/Exchange?market=FJC_BTC'];
        $market['atomicdex'] = ['url' => 'https://atomicdex.io/'];
        $market['northern'] = ['url' => 'https://nortexchange.com/exchange/?market=FJC_BTC'];
        $market['blockbid'] = ['url' => 'https://www.blockbid.io/'];

        return $this->render('other/home.html.twig', [
            'market' => $market
        ]);
    }

    public function termsOfUse(): Response
    {
        return $this->render('other/terms_of_use.html.twig');
    }
    
    public function privacyPolicy(): Response
    {
        return $this->render('other/privacy_policy.html.twig');
    }

    public function walletGenerator(): Response
    {
        return $this->render('other/wallet_generator.html.twig');
    }

    public function api(JsonRpcClient $fujicoin, ApiUtils $utils, $value): Response
    {
        $txoutsetinfo = $fujicoin->gettxoutsetinfo();
        $getmininginfo = $fujicoin->getmininginfo();
        $networkhashps = $getmininginfo['networkhashps'] / 1000000;

        // Array to do a loop in the twig file -> less code in this last one
        switch ($value) {
            case "height":
                $render_value = $txoutsetinfo['height'];
                break;
            case "block_reward":
                $render_value = $utils->reward($txoutsetinfo['height']);
                break;
            case "total_amount":
                $render_value = $txoutsetinfo['total_amount'];
                break;
            case "difficulty":
                $render_value = $getmininginfo['difficulty'];
                break;
            case "hashespersec":
                $render_value = $networkhashps;
                break;
            default:
                $render_value = "undefined";
        }
        return $this->render('other/api.html.twig', [
            'value_api' => $render_value]);
    }
}
